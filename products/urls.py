from unicodedata import name
from venv import create
from django.urls import URLPattern, path
from products.forms import ProductsTypeForm
from products.models import Products
from .views import products_list, create_product, update_product, delete_product, productstypes_list, productstypes_create, \
    ProductsAPIView


urlpatterns = [
    path('product/', products_list, name="products_list"),
    path('create-product/', create_product, name="create_product"),
    path('update-product/<int:id>/', update_product, name="update_product"),
    path('delete-product/<int:id>/  ', delete_product, name="delete_product"),

    path('products-types/', productstypes_list, name="product_types"),
    path('productstype_create/', productstypes_create, name="product_type_create"),
    # path('productstype_update/<int:id>/', productstypes_update, name="update_product_type")

    # API views
    path('products-api/', ProductsAPIView.as_view(), name="products_api")

]