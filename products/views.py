from asyncio.format_helpers import _format_callback
from itertools import product
from multiprocessing import context
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import TemplateView, ListView, CreateView, UpdateView, DeleteView, DetailView
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from .models import Products, ProductTypes
from .forms import ProductsForm, ProductsTypeForm
from django.utils.timezone import now

from .serializers import ProductSerializer
from rest_framework.views import APIView
from rest_framework.response import Response


today = now().strftime("%Y-%m-%d")

#_______________________   Products Start   ___________________________


def products_list(request):
    products = Products.objects.all()
    context = {
        "products": products,
        "user": request.user
    }

    return render(request, "products.html", context)


def create_product(request):
    form = ProductsForm()

    if request.method == "POST":
        form = ProductsForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            data = form.save(commit=False)
            data.created_at = today
            data.save()
            return redirect("products_list")

    context = {
        "form": form
    }

    return render(request, "product_create.html", context)
    

def update_product(request, id):
    model = Products.objects.get(id=id)
    form = ProductsForm(instance=model)
    if request.method == "POST":
        form = ProductsForm(data=request.POST, instance=model)

        if form.is_valid():
            form.save()
            return redirect("products_list")

    context = {
        "form": form,
        "id": id
    }

    return render(request, "update_product.html", context)


def delete_product(request, id):
    model = Products.objects.get(id=id)
    if request.method == "POST" and model:
        model.delete()
        return redirect("products_list")
    
    context = {
        "id": id
    }
    return render(request, 'delete_product.html', context)

#_______________________   Products End   ___________________________

#____________________   Product Types Start   ________________________

def productstypes_list(request):
    products_types = ProductTypes.objects.all()
    context = {
        "products_types": products_types,
    }

    return render(request, 'products_types.html', context)


def productstypes_create(request):
    form = ProductsTypeForm()

    if request.method == "POST":
        form = ProductsTypeForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            data = form.save(commit=False)
            data.created_at = today
            data.save()
            return redirect("product_types")

    context = {
        "form": form
    }

    return render(request, "product_types_create.html", context)


def update_product(request, id):
    model = Products.objects.get(id=id)
    form = ProductsForm(instance=model)
    if request.method == "POST":
        form = ProductsForm(data=request.POST, instance=model)

        if form.is_valid():
            form.save()
            return redirect("products_list")

    context = {
        "form": form,
        "id": id
    }

    return render(request, "update_product.html", context)


class ProductsAPIView(APIView):
    serializer_class = ProductSerializer
    model = Products

    def get(self, request, *args, **kwargs):
        products = self.model.objects.all()
        serializer = self.serializer_class(products, many=True)

        return Response(serializer.data)

