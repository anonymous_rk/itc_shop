from rest_framework import serializers
from .models import Products, ProductTypes


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Products
        fields = [
            'id',
            'product_type',
            'price',
            'name',
            'created_at',
            'updated_at'
        ]

    # def create(self, validated_data):


