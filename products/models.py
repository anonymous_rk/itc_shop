from django.db import models
from django.utils.timezone import now
from django.core.exceptions import ValidationError

TODAY = now()


def image_upload_to(instance, filename):
    print("filename: ", filename)
    extension = filename.split('.')[-1]
    if extension not in ['jpg', 'jpeg', 'png']:
        raise ValidationError("Image format is not supported.")

    return "products/user_image_" + str(TODAY) + "." + extension


class ProductTypes(models.Model):
    name = models.CharField(max_length=100)
    created_at = models.DateField(null=True, blank=True)
    updated_at = models.DateField(null=True, blank=True)

    def __str__(self) -> str:
        return self.name


class Products(models.Model):
    product_type = models.ForeignKey(ProductTypes, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    price = models.FloatField()
    image = models.ImageField(upload_to=image_upload_to)
    created_at = models.DateField(null=True, blank=True)
    updated_at = models.DateField(null=True, blank=True)

    def __str__(self) -> str:
        return self.name


