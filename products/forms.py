from cProfile import label
from django import forms
from .models import ProductTypes, Products


class ProductsForm(forms.ModelForm):

    class Meta:
        model = Products
        fields = [
            'product_type',
            'name',
            'price',
            'created_at',
            'updated_at'
        ]

        labels = {
            "product_type":'Turi',
            'name': 'Nomi',
            'price': 'Narxi'
        }


class ProductsTypeForm(forms.ModelForm):
    class Meta:
        model = ProductTypes
        fields = [
            'name',
            'created_at',
            'updated_at'
        ]

        labels = {
            'name': 'Mahsulot turi'
            }