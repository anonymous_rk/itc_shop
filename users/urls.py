from django.urls import path, include
from .views import CreateCustomUser, LoginUser, logout_user, profile, image_save


urlpatterns = [
    path('register/', CreateCustomUser.as_view(), name='register'),
    path('login/', LoginUser.as_view(), name='login'),
    path('logout', logout_user, name='logout'),
    path('profile/<int:id>/', profile, name='profile'),
    path('avatar/', image_save, name='avatar'),
]
