from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import AbstractUser
from django.utils.timezone import now
from random import randint, randrange


TODAY = now().strftime("%d_%m_%Y_%H_%M")


def user_image_upload_to(instance, filename):
    extension = filename.split('.')[-1]
    if extension not in ['jpg', 'jpeg', 'png']:
        raise ValidationError("Image format is not supported.")

    return "users/user_image_" + str(TODAY) + "." + extension


class CustomUser(AbstractUser):
    ROLES = (
        ("ADMIN", "ADMIN"),
        ("ORGANIZATION", "ORGANIZATION"),
        ("CUSTOMER", "CUSTOMER")
    )

    role = models.CharField(max_length=20, choices=ROLES)
    image = models.ImageField(upload_to=user_image_upload_to)
