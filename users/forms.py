
from django.forms import TextInput, ModelForm, Form, CharField, PasswordInput,  Select
from .models import CustomUser
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm


class CustomUserForm(UserCreationForm, ModelForm):
    
    
    class Meta:
        model = CustomUser
        fields = [
            'username',
            'email',
            'password1',
            'password2',
            'role'
        ]

        widgets = {
            'username':TextInput(attrs={"style": 'border-radius: 50px; height: 45px; width: 300px; padding-left: 20px; border-color: transparent; background-color: #a9a9a942', "placeholder": 'Username'}),
            'email':TextInput(attrs={"style": 'border-radius: 50px; height: 45px; width: 300px; padding-left: 20px; border-color: transparent; background-color: #a9a9a942', "placeholder": 'Email'}),
            'role':Select(attrs={"style": 'border-radius: 50px; height: 45px; border-color: transparent; background-color: #a9a9a942; padding-left: 20px'}),
        }


        labels = {
         
        }
        


class CustomLoginForm(Form):
    username = CharField(max_length=100, widget=TextInput(attrs={"style": 'border-radius: 50px; height: 45px; width: 300px; padding-left: 20px; border-color: transparent; background-color: #a9a9a942', "placeholder": 'Username'}))
    password = CharField(max_length=255, widget=PasswordInput(attrs={"style": 'border-radius: 50px; height: 45px; width: 300px; padding-left: 20px; border-color: transparent; background-color: #a9a9a942', "placeholder": 'Password'}))