import json
from django.shortcuts import redirect, render
from django.urls import reverse
from django.contrib.auth import login, logout, authenticate
from django.views.generic import TemplateView, ListView, CreateView, UpdateView, DeleteView, DetailView, View
from .models import CustomUser
from .forms import CustomUserForm, CustomLoginForm
from django.http.response import JsonResponse, HttpResponse
from django.core.files.images import ImageFile


class CreateCustomUser(CreateView):
    model = CustomUser
    template_name = "register_users/register.html"
    form_class = CustomUserForm

    def get_success_url(self) -> str:
        return reverse('home')


class LoginUser(TemplateView):
    model = CustomUser
    template_name = "register_users/login.html"
    form_class = CustomLoginForm

    def get_success_url(self):
        return reverse('home')

    def get(self, request):
        form = self.form_class()
        return render(request, self.template_name, context={'form': form})

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password'],
            )
            if user is not None:
                login(request, user)
                return redirect('home')
        message = 'False username or password'
        return render(request, self.template_name, context={'form': form, 'message': message})


def logout_user(request):
    logout(request)
    return redirect("home")


def profile(request, id):

    return render(request, 'register_users/profile.html', {"id": id})


def image_save(request):
    user = CustomUser.objects.get(id=request.user.id)
    user.image = ImageFile(request.FILES["userImage"])
    user.save()

    context = {
        "msg": "success",
        "image": user.image.url
    }
    return JsonResponse(context, status=200)
    # except Exception as e:
    #     return JsonResponse({"msg": "fail", "detail": e.args[0]}, status=400)

