import json
import requests
from django.shortcuts import render
from products.models import Products, ProductTypes
from django.http import JsonResponse
from django.utils.timezone import now
from users.models import CustomUser
from django.db.models import Q
from django.utils import translation


def get_currency():
    url = "http://cbu.uz/ru/arkhiv-kursov-valyut/json/?base=USD"
    cb_data = requests.get(url)

    cb_json_data = json.loads(cb_data.text)
    central_bank = []
    for json_data in cb_json_data:
        if '.' not in json_data["Diff"]:
            json_data["Diff"] = int(json_data["Diff"])
        json_data["Diff"] = float(json_data["Diff"])

        if json_data["Ccy"] == 'USD' or json_data["Ccy"] == "CAD" or json_data["Ccy"] == "RUB" or json_data[
            "Ccy"] == "CHF" or json_data["Ccy"] == "EUR":
            central_bank.append(json_data)

    return central_bank


def home_view(request):
    products = Products.objects.all()
    product_types = ProductTypes.objects.all()
    type_id = request.GET.get("type_id")

    if type_id:
        products = products.filter(product_type_id=type_id)
    context = {
        'products': products,
        'types': product_types,
        'currencies': get_currency()
    }

    return render(request, 'home.html', context)


def products_filter(request):
    type_id = request.GET.get("type_id")
    products = Products.objects.filter(product_type_id=type_id).values()
    context = {
        'products': list(products)
    }
    print(context)
    return JsonResponse(context, safe=False)


def products_filter_by_date(request):
    date = request.GET.get("date_type")
    products = None
    print(date)
    if date == "1":
        products = Products.objects.filter(created_at=now()).values() # created_at__range=[]
    elif date == "2":
        products = Products.objects.filter(created_at__range=[f"2022-04-{now().day-7}", now()]).values()
    elif date == "3":
        products = Products.objects.filter(created_at__range=[f"2022-{now().month-1}-{now().day}", now()]).values() # bugungi sanadan ortga qarab bir oy muddat

    print(len(products))
    context = {
        'products': list(products)
    }
    print(context)
    return JsonResponse(context, safe=False)


def profile_view(request):
    user = request.user.id


def search_view(request):
    search_value = request.GET.get('search')
    products = Products.objects.filter(Q(name__iexact=search_value) | Q(name__contains=search_value) | Q(product_type__name__contains=search_value) | Q(product_type__name__iexact=search_value)).values()

    context = {
        'products': list(products)
    }

    return JsonResponse(context, safe=False)




