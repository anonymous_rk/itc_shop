from django.urls import path
from .views import *


urlpatterns = [
    path('home/', home_view, name="home"),
    path('search/', search_view, name="search_view"),
    path('filter-products/', products_filter, name="get_products"), #http://.../filter-products/?type_id=3
    path('filter-products-date/', products_filter_by_date, name="get_products_by_date"), #http://.../filter-products/?type_id=3
    path('search/', search_view, name="search_view")
]
