from itertools import product
from django.shortcuts import redirect, render
from .models import Cart
from .forms import GeeksForm
from products.models import Products


def create_view(request):

    product_id = request.POST.get("product_id")
    form = GeeksForm()
    product = Products.objects.get(id=product_id)

    if product is not None:
        cart = Cart.objects.create(
            product=product,
            customer=request.user
        )
        cart.save()
        return redirect('cart_list')

    context = {
        "form": form
    }
    return render(request, "create_view.html", context)


# logika

def cart_list(request):
    cart = Cart.objects.all()
    if request.GET.get("filter"):
        cart = Cart.objects.filter(customer_id=request.user.id)
    context = {
        "dataset": cart
    }
    return render(request, "list_view.html", context)


def detail_view(request, id):
    context = {"data": Cart.objects.get(id=id)}

    return render(request, "detail_view.html", context)


def update_view(request, id):
    obj = get_object_or_404(Cart, id=id)
    form = GeeksForm(request.POST, instance=obj)

    if form.is_valid():
        form.save()
        return redirect("cart_list")

    context = {
        "form": form,
        "id": id
    }

    return render(request, "update_view.html", context)


def delete_view(request, id):
    context = {
        "id": id
    }

    obj = get_object_or_404(Cart, id=id)

    if request.method == "POST":
        # delete object
        obj.delete()
        return redirect("cart_list")

    return render(request, "delete_view.html", context)
