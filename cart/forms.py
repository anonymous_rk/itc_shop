from django import forms
from .models import Cart



class GeeksForm(forms.ModelForm):

	class Meta:
		model = Cart

		fields = [
			"product",
			"customer",
		]
