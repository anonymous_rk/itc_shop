from django.db import models
from products.models import Products
from users.models import CustomUser


class Cart(models.Model):

	product = models.ForeignKey(Products,on_delete=models.CASCADE)
	customer = models.ForeignKey(CustomUser,on_delete=models.CASCADE)
	created_at = models.DateField(null=True, blank=True)
	updated_at = models.DateField(null=True, blank=True)

	def __str__(self):
		return self.product.name + "-" + self.customer.username

