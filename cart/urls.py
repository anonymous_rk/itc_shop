from django.urls import path
from .views import create_view, delete_view,cart_list, update_view


urlpatterns = [
    path('cart-list/',cart_list, name="cart_list"),
	path('delete/<int:id>/', delete_view, name="delete_order"),
	path('create-view/', create_view, name="create_cart"),
    # path('detail-view/<int:id>/', update_product, name="detail_view"),
    path('update-view/<int:id>/', update_view, name="update_view")
]
