from django import template
from django.utils.translation import get_language
from django.conf import settings

register = template.Library()

@register.tags
def get_url_tag(request, lang):
    if lang:
        active_language = get_language()
        return request.path.replace(active_language, lang, 1)
    return request.path
