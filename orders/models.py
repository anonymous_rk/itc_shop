from django.db import models
from products.models import Products
from users.models import CustomUser


class Orders(models.Model):
    DELIVERY_STATUS = (
        ("PENDING", "PENDING"),
        ("ON_DELIVERY", "ON_DELIVERY"),
        ("DELIVERED", "DELIVERED")
    )

    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    total_amount = models.IntegerField()
    total_price = models.IntegerField()
    is_delivered = models.BooleanField(default=False)
    created_at = models.DateField(null=True, blank=True)
    updated_at = models.DateField(null=True, blank=True)

